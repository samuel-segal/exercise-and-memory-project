const recallMorphemes = function(morphemeObject){
	continueButton.hidden = true
	wordInput.hidden = false
	
	//Shuffles the array
	let keys = shuffleArray(Object.keys(morphemeObject))
	
	
	
	let currKey = keys.pop()[0];
	
	setContentText(currKey)

	const onEnd = function(){
		wordInput.hidden = true
		continueButton.hidden = false
	}

	const onEnter = function (){
		const text = wordInput.value
		
		//TODO Improve logging system
		experimentLog.logMorphemeRecall(currKey,morphemeObject[currKey],text)
		
		if(keys.length>0){
			currKey = keys.pop()[0]
			setContentText(currKey)
		}
		else{
			onEnd()
		}

		//Clears text input
		wordInput.value = ''
	}

	wordInput.onkeyup = function (event){
		if(event.key=='Enter'){
			onEnter()
		}
	}


}