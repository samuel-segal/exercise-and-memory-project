let partIndex = 0;
continueButton.onclick = function(){
	changePart(partIndex)
	partIndex++
}
const changePart = function(part){
	switch(part){

	case 0:
		setContentText('Hello, and welcome to the experiment')
		break
	case 1:
		setContentText('This experiment will consist of memorization, as well as some light aerobic exercise')
		break
	case 2:
		setContentText('In certain sections, you will be asked to count how many heart beats you have over a short period of time. Please count your heartbeats by putting two fingers to your temple.')
		break
	case 3:
		setContentText('In those intervals, the text will turn red, and you will be presented with a countdown. You do not need to keep track of the time yourself.')
		break
	case 4:
		setContentText('Click the button when you are ready to start measuring your resting heart rate')
		setButtonText('Ready')
		break
	case 5:
		measureHeartRate()
		break
	case 6:
		enterHeartRate(true)
		setButtonText('Next')
		break
	case 7:
		setContentText('Great! In a little bit, you will be shown a series of word pairings')
		break
	case 8:
		//TODO Maybe make this appear with which
		setContentText('Your job is to memorize which words appear with which. You will then be prompted with the word on the left, asking what the word on the right was.')
		break
	case 9:
		setContentText('Click the button when you are ready')
		setButtonText('Ready')
		break
	case 10:
		performMorphemes(firstMorphemes)
		break
	case 11:
		setContentText('Click the button when you are ready to show which words you remember (If you don\'t remember at all, you can just press enter with an empty box)')
		break
	case 12:
		recallMorphemes(firstMorphemes)
		break
	case 13:
		setContentText('Great! Now we move on to the exercise portion');
		setButtonText('Next')
		break
	case 14:
		setContentText('You are going to do a series of jumping jacks, measure your heart rate, and then do the same memorization procedure again with different words')
		break
	case 15:
		setContentText('Click the button when you are ready to start jumping jacks')
		setButtonText('Ready')
		break
	case 16:
		aerobics()
		setButtonText('Next')
		break
	case 17:
		measureHeartRate()
		break
	case 18:
		enterHeartRate(false)
		break
	case 19:
		setContentText('Now please memorize these word pairings')
		setButtonText('Ready')
		break
	case 20:
		performMorphemes(secondMorphemes)
		break
	case 21:
		setContentText('Click the button when you are ready to show which words you remember')
		break
	case 22:
		recallMorphemes(secondMorphemes)
		break
	default:
		end()
	}
}
