const numMorphemeTime = 3000
const jumpingJackTime = 30
const heartBeatCountTime = 15

const displayText = document.getElementById('display-text')
const displaySub = document.getElementById('subtitle')
const displayLink = document.getElementById('link')
const scrollBox = document.getElementById('scroll-box')
const scrollContent = document.getElementById('scroll-content')
const continueButton = document.getElementById('continue-button')
const wordInput = document.getElementById('word-input')

const setContentText = function(text){
	displayText.innerText = text
}
const setSubText = function(text){
	displaySub.innerText = text
}
const setScrollContentText = function(text){
	scrollContent.innerText = text
}

const setButtonText = function(text){
	continueButton.innerText = text
}

const countdown = function(time,endCallback){
		
	setSubText(time)
	
	if(time<=0){
		endCallback()
	}
	else{
		setTimeout(()=>{
			countdown(time-1,endCallback)
		},1000)
	}
}

const mainCountdown = function(time,endCallback){

	
	setContentText(time)

	if(time<=0){
		endCallback()
	}
	else{
		setTimeout(()=>{
			mainCountdown(time-1,endCallback)
		},1000)
	}
}

//Has side effect, but that's alright for our purposes
const shuffleArray = function(array){
	let newArray = []
	while(array.length>0){
		const index = Math.floor(Math.random()*array.length)
		const val = array.splice(index,1)
		newArray.push(val)
	}
	return newArray
}