const measureHeartRate = function(){

	const onEnd = function(){
		displayText.style.color = 'black'
		displaySub.hidden = true
		continueButton.hidden = false
	}

	continueButton.hidden = true
	
	mainCountdown(3,()=>{
		displayText.style.color = 'red'
		displaySub.hidden = false
		setContentText('Count heart beats')
		countdown(heartBeatCountTime,onEnd)
	})
	
}