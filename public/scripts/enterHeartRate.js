const enterHeartRate = function(isResting){
	continueButton.hidden = true
	wordInput.hidden = false

	setContentText('How many heart beats did you count?')

	const onEnd = function(){
		continueButton.hidden = false
		wordInput.hidden = true
		wordInput.value = ''
	}

	//Maybe this sort of thing belongs in changeHelper?
	const onEnter = function(){
		const heartInput = wordInput.value
		
		//Validates input
		const num = parseInt(heartInput)
		if(!isNaN(heartInput)&&num){
			//Verbose for readability
			if(isResting){
				experimentLog.logRestHeartBeatCount(num)
			}
			else{
				experimentLog.logExerciseHeartBeatCount(num)
			}
			onEnd()
		}
	}

	wordInput.onkeyup = function(event){
		if(event.key=='Enter'){
			onEnter()
		}
	}
}