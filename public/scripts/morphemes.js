//All morphemes are 1 syllable to avoid inconsistencies
//These are randomly chosen via https://randomwordgenerator.com/
const firstMorphemes={
	"plant": "guest",
	"code": "bell",
	"help": "breast",
	"view": "eat",
	"mine": "shave",
	"me": "braid",
	/*"leak": "great",
	"tool": "slap",
	"growth": "spit",
	"brain": "stream",
	"stroke": "doll",
	"pool": "maze",
	"miss": "harm",
	"last": "brush",
	"way": "sink",
	"ice": "gap",
	"forge": "scratch",
	"ward": "leash",
	"spoil": "plane",
	"walk": "wheel"*/
}

const secondMorphemes = {
	"punch": "pour",
	"blame": "team",
	"coast": "pair",
	"state": "chew",
	"wing": "crash",
	"lamb": "heel",
	/*"meet": "bird",
	"claim": "dream",
	"snap": "harsh",
	"school": "crowd",
	"slow": "high",
	"move": "boy",
	"cream": "text",
	"straight": "bend",
	"tin": "treat",
	"leg": "skilled",
	"lamp": "sense",
	"soap": "bless",
	"youth": "spare",
	"rack": "flawed",*/
}


//Just to ensure that there aren't any repeats
const validateMorphemeObjects = function(){

	const keys1 = Object.keys(firstMorphemes)
	const values1 = Object.values(firstMorphemes)
	const keys2 = Object.keys(secondMorphemes)
	const values2 = Object.values(secondMorphemes)

	//Ensures that both of them are the same size
	if(keys1.length!=keys2.length) throw new Error('Morphemes objects are unequal sizes')
	
	//Concatenates all arrays to assure no duplicates
	const masterArray = keys1.concat(values1,keys2,values2)
	//Checks for duplicates
	masterArray.forEach( (e, i)=>{
		for(++i;i<masterArray.length;i++){
			if(masterArray[i]==e) throw new Error(`Duplicate value ${e}`)
		}
	})

	return true;
}