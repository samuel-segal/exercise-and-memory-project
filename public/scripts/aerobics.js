const aerobics = function(){
	

	const onEnd = function(){
		displayText.style.color = 'black'
		displaySub.hidden = true
		continueButton.hidden = false
	}

	continueButton.hidden = true

	mainCountdown(3,()=>{
		displaySub.hidden = false
		setContentText('Do jumping jacks')
		countdown(jumpingJackTime,onEnd)
	})
	
}