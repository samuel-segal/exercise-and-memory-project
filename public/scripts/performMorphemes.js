const performMorphemes = function(morphemeObject){
	
	continueButton.hidden = true

	experimentLog.logMorphemeStart()
	//Shuffles the array
	let keyArr = shuffleArray( Object.keys(morphemeObject) )

	const onEnd = function(){
		setButtonText('Next')
		continueButton.hidden = false
	}

	const timeoutFunc = function(){

		const currKey = keyArr.pop()[0]
		const currVal = morphemeObject[currKey]

		experimentLog.logMorphemePerform(currKey,currVal)
		setContentText(`${currKey}:\t${currVal}`)

		if(keyArr.length>0){
			setTimeout(timeoutFunc,numMorphemeTime)
		}
		else{
			onEnd()
		}
	}

	timeoutFunc();
}
