class MorphemeRecallLog{
	constructor(){
		this.keyValues = []
		this.keyValueResponses = []
	}

	logKeyValue(key,value){
		this.keyValues.push(
			{
				"time": new Date().getTime(),
				"key": key,
				"value": value
			}
		)
	}

	logKeyValueResponse(key,value,returnedReponse){
		this.keyValueResponses.push(
			{
				"time": new Date().getTime(),
				"key": key,
				"value": value,
				"returnedResponse": (returnedReponse.length>0?returnedReponse:'NULL')
			}

		)
	}
}

class Logger{

	constructor(){
		this.morphemeRecallArr = []
	}

	logRestHeartBeatCount(beat){
		this.restHeartBeat = beat
	}

	logExerciseHeartBeatCount(beat){
		this.exerciseHeartBeat = beat
	}

	logMorphemeStart(){
		this.morphemeRecallArr.push(new MorphemeRecallLog())
	}

	logMorphemePerform(key,value){
		const currMorpheme = this.morphemeRecallArr[this.morphemeRecallArr.length-1]
		currMorpheme.logKeyValue(key,value)
	}

	logMorphemeRecall(key,value,returnedResponse){
		const currMorpheme = this.morphemeRecallArr[this.morphemeRecallArr.length-1]
		currMorpheme.logKeyValueResponse(key,value,returnedResponse)
	}

	

	toString(){
		return JSON.stringify(this)
	}

}

const experimentLog = new Logger()