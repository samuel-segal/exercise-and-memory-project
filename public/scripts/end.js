const end = function(){
	continueButton.hidden = true
	displayLink.hidden = false
	scrollBox.style.display = 'block'
	setScrollContentText(experimentLog.toString())

	setContentText('End of experiment. Please copy the below data, and paste it into the Google form below. Thank you so much for participating. ')
}